package main.java.com.game.drizzle.play;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL11;

import main.java.com.game.drizzle.entities.Entity;
import main.java.com.game.drizzle.entities.EntityRaindrop;
import main.java.com.game.drizzle.input.TouchEvent;
import main.java.com.game.drizzle.manager.GameManager;
import main.java.com.game.drizzle.manager.Screen;
import main.java.com.game.drizzle.utils.Renderer;
import main.java.com.game.drizzle.utils.Texture;
import android.content.Context;
import android.content.res.AssetManager;
import android.hardware.SensorEvent;
import android.opengl.GLES11;
import android.view.MotionEvent;

public class Play implements Screen {
	private static final String TAG = "PlayScreen";

    private Context mContext;
    private Texture mTexture;
    private Entity mRaindrop;
    private ArrayList<Entity> mEntities = new ArrayList<Entity>();
    private Background mBackground;
    private PlayHUD mPlayHUD;
    private boolean isPaused = false;
    
	@Override
	public void initializeScreen(Context context) {
		mContext = context;

		AssetManager assets = mContext.getAssets();
        mTexture = Texture.createTextureFromFile("graphics.png", assets);
        
        mBackground = new Background(assets);
		
        mPlayHUD = new PlayHUD(mTexture);
        
		mRaindrop = new EntityRaindrop();
		mRaindrop.setXY(100, GameManager.sSurfaceHeight - 500);
		
		mEntities.add(mRaindrop);
	}
	
	@Override
	public void unloadScreen() {
		mBackground.unload();
		GLES11.glDeleteTextures(1, new int[] { mTexture.getId() }, 0);
	}

	@Override
	public void handleSensorInput(List<SensorEvent> events) {
		int evtSize = events.size();
		int entSize = mEntities.size();
		Entity ent;
		SensorEvent evt;
		for(int i=0; i<entSize; i++) {
			ent = mEntities.get(i);
			for(int j=0; j<evtSize; j++) {
				evt = events.get(j);
				ent.processSensorEvent(evt);
			}
		}
	}
	
	@Override
	public void handleTouchInput(List<TouchEvent> events) {
		int evtSize = events.size();
		int entSize = mEntities.size();
		Entity ent;
		TouchEvent evt;
		for(int i = 0; i < evtSize; i++) {
			evt = events.get(i);
			for(int j=0; j<entSize; j++) {
				ent = mEntities.get(j);				
				ent.processTouchEvent(evt);
			}
		}
	}
	
	@Override
	public void updateScreen(float dt) {
		if(!isPaused) {
			// update HUD display
			mPlayHUD.update(dt);
			
			// update background
			mBackground.update(dt);
			
			// update entities
			int size = mEntities.size();
			for (int i = 0; i < size; i++) {
				mEntities.get(i).update(dt);
			}
		}
	}
	
	@Override
	public void drawToScreen(Renderer renderer) {
		// draw background
		mBackground.draw(renderer);
		
		// draw entities
		GLES11.glBindTexture(GL11.GL_TEXTURE_2D, mTexture.getId());
		int size = mEntities.size();
		for (int i = 0; i < size; i++) {
			renderer.draw(mEntities.get(i));
		}
        
        // draw HUD
        mPlayHUD.draw(renderer);
	}

	@Override
	public void handleBackEvent() {
		
		isPaused = true;
	}
	
}
