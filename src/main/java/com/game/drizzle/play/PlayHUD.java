package main.java.com.game.drizzle.play;

import javax.microedition.khronos.opengles.GL11;

import main.java.com.game.drizzle.entities.EntityNumbers;
import main.java.com.game.drizzle.utils.Renderer;
import main.java.com.game.drizzle.utils.Texture;
import android.opengl.GLES11;


public class PlayHUD {
	private Texture mTexture;
	private EntityNumbers mScore;
	private float mDelay = 0;
	
	public PlayHUD(Texture texture) {
		mTexture = texture;
		mScore = new EntityNumbers();
		mScore.setValue(0);
	}
	
	public void update(float dt) {
		mDelay -= dt;
		if(mDelay < 0) {
			mScore.addValue(1);
			mDelay = 10;
		}
	}

	public void draw(Renderer renderer) {
		renderer.bindTexture(mTexture);
		mScore.draw(renderer);
	}
	
	public int getScore() {
		return mScore.getValue();
	}

	public void setScore(int score) {
		mScore.setValue(score);
	}
}
