package main.java.com.game.drizzle.play;

import main.java.com.game.drizzle.utils.Renderer;
import main.java.com.game.drizzle.utils.Texture;
import android.content.res.AssetManager;
import android.opengl.GLES11;

public class Background {
	private Texture mTexture;
	private int[] mRegion;
	private int mDistanceY = 0;
	
	public Background(AssetManager assets) {
		mTexture = Texture.createTextureFromFile("city.png", assets);
		mRegion = new int[] { 270, 960, 270, -960 };
	}

	public void update(float dt) {
		mDistanceY += 15;
	}

	public void draw(Renderer renderer) {
		renderer.bindTexture(mTexture);
		int dy = mDistanceY % 1920;
		renderer.draw(mRegion, 0, dy, 1080, 1920);
		renderer.draw(mRegion, 0, dy - 1920, 1080, 1920);
	}
	
	public void unload() {
		GLES11.glDeleteTextures(1, new int[] { mTexture.getId() }, 0);		
	}
}
