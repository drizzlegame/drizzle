package main.java.com.game.drizzle;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import main.java.com.game.drizzle.manager.GameManager;
import main.java.com.game.drizzle.menu.MainMenu;
import main.java.com.game.drizzle.utils.Logger;
import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity implements SensorEventListener
{
	private class GameRenderer implements Renderer
	{
	    private static final String TAG = "GameRenderer";
	    
	    @Override
	    public void onDrawFrame(GL10 gl)
	    {
	        mGame.onDrawFrame();
	    }

	    @Override
	    public void onSurfaceCreated(GL10 gl, EGLConfig config)
	    {
	        Logger.logD(TAG, "onSurfaceCreated");
	        mGame.onSurfaceCreated(gl, config);
	    }

	    @Override
	    public void onSurfaceChanged(GL10 gl, int width, int height)
	    {
	        Logger.logD(TAG, "onSurfaceChanged");

	        Display display = getWindowManager().getDefaultDisplay();
	        GameManager.sDeviceWidth = display.getWidth();
	        GameManager.sDeviceHeight = display.getHeight();
	        
	        mGame.load(width, height);
	        mGame.initializeScreen();
	    }
	}
	
	private class GameSurfaceView extends GLSurfaceView 
	{
		public GameSurfaceView(Context context)
	    {
	        super(context);
	        setRenderer(new GameRenderer());
	    }
		
		@Override
		public boolean onTouchEvent(MotionEvent event) 
		{
			mGame.handleInput(event);
			return true;
		}
	}
	
    private static final String TAG = "GameActivity";

	private static final String MAIN_MENU = "MainMenu";
    
    private GameManager mGame;
    private GLSurfaceView mGLView;
    private SensorManager mSensorManager;
	private Sensor mSensor;

	@Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Logger.logD(TAG, "onCreate");
        
        setFullScreen();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);;
        
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        
        mGame = GameManager.getInstance();
        mGame.setContext(this);
        mGame.setCurrentScreen(new MainMenu());
        
        mGLView = new GameSurfaceView(this);
        setContentView(mGLView);
    }
	
    @Override
    protected void onPause()
    {
        super.onPause();
        Logger.logD(TAG, "onPause");
        mSensorManager.unregisterListener(this);
        mGLView.onPause();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Logger.logD(TAG, "onResume");
    	mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_GAME);
    	mGLView.onResume();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Logger.logD(TAG, "onDestroy");
    }
    
    private void setFullScreen()
    {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        window.requestFeature(Window.FEATURE_NO_TITLE);
    }
    
    @Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) 
	{
		// TODO Auto-generated method stub
	}
	
	@Override
    public void onSensorChanged(SensorEvent event)
    {
		mGame.handleInput(event);
    }
	
	@Override
	public void onBackPressed() {
		String currentScreen = mGame.getCurrentScreen().getClass().getSimpleName();
		if (currentScreen.equalsIgnoreCase(MAIN_MENU)) {
			super.onBackPressed();
		} else {
			mGame.onBackPressed();
		}
	}
}