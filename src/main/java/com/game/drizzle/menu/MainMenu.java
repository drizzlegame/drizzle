package main.java.com.game.drizzle.menu;

import java.util.ArrayList;
import java.util.List;

import main.java.com.game.drizzle.entities.Entity;
import main.java.com.game.drizzle.entities.EntityButton;
import main.java.com.game.drizzle.entities.EntityMainMenu;
import main.java.com.game.drizzle.input.TouchEvent;
import main.java.com.game.drizzle.manager.Screen;
import main.java.com.game.drizzle.utils.Renderer;
import main.java.com.game.drizzle.utils.Texture;
import android.content.Context;
import android.content.res.AssetManager;
import android.hardware.SensorEvent;
import android.opengl.GLES11;
import android.view.MotionEvent;

public class MainMenu implements Screen {
	private static final String TAG = "MainMenu";

	private Context mContext;
	private Texture mTexture;
    private ArrayList<Entity> mEntities = new ArrayList<Entity>();

	@Override
	public void initializeScreen(Context context) {
		mContext = context;
		AssetManager assets = mContext.getAssets();
        mTexture = Texture.createTextureFromFile("mainmenu/menu.png", assets);
        mEntities.add(new EntityMainMenu());
        
        mEntities.add(new StartButton(1187, 17, 796, 195, 160, 950, this));
        mEntities.add(new OptionsButton(1187, 433, 796, 195, 160, 950 - 1 * 250));
        mEntities.add(new ScoreBoardButton(1187, 226, 796, 195, 160, 950 - 2 * 250));
        mEntities.add(new InfoButton(1190, 672, 162, 158, 800, 1200));
	}

	@Override
	public void unloadScreen() {
		GLES11.glDeleteTextures(
				1, 
				new int[] {
						mTexture.getId(),
				}, 
				0);
	}

	@Override
	public void drawToScreen(Renderer renderer) {
		// draw entities
		renderer.bindTexture(mTexture);
        for(Entity e : mEntities) {
            if(e.isVisible()) {
            	renderer.draw(e);
            }
        }
	}

	@Override
	public void updateScreen(float dt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleSensorInput(List<SensorEvent> events) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleTouchInput(List<TouchEvent> events) {
		int evtSize = events.size();
		int entSize = mEntities.size();
		Entity ent;
		TouchEvent evt;
		for(int i = 0; i < evtSize; i++) {
			evt = events.get(i);
			for(int j = 0; j < entSize; j++) {
				ent = mEntities.get(j);				
				if(ent.processTouchEvent(evt)) return;
			}
		}
	}

	@Override
	public void handleBackEvent() {
		// TODO Auto-generated method stub
		
	}
}
