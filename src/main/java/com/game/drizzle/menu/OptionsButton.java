package main.java.com.game.drizzle.menu;

import main.java.com.game.drizzle.entities.EntityButton;
import main.java.com.game.drizzle.utils.Logger;

public class OptionsButton extends EntityButton {

	public OptionsButton(int x0, int y0, int w, int h, float x, float y) {
		super(x0, y0, w, h, x, y);
	}

	@Override
	protected boolean onReleased() {
		// TODO: implement
		Logger.logD("Options Button", "Launch Options Screen");
		return true;
	}

}
