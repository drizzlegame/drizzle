package main.java.com.game.drizzle.menu;

import main.java.com.game.drizzle.entities.EntityButton;
import main.java.com.game.drizzle.manager.GameManager;
import main.java.com.game.drizzle.play.Play;

public class StartButton extends EntityButton {

	private MainMenu mScreen;

	public StartButton(int x0, int y0, int w, int h, float x, float y, MainMenu screen) {
		super(x0, y0, w, h, x, y);
		mScreen = screen;
	}

	@Override
	protected boolean onReleased() {
		mScreen.unloadScreen();
		GameManager mgr = GameManager.getInstance();
		mgr.setCurrentScreen(new Play());
		mgr.initializeScreen();
		return true;
	}

}
