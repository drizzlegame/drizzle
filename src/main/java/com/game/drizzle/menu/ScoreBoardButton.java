package main.java.com.game.drizzle.menu;

import main.java.com.game.drizzle.entities.EntityButton;
import main.java.com.game.drizzle.utils.Logger;

public class ScoreBoardButton extends EntityButton {

	public ScoreBoardButton(int x0, int y0, int w, int h, float x, float y) {
		super(x0, y0, w, h, x, y);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected boolean onReleased() {
		// TODO: implement
		Logger.logD("Scoreboard Button", "Launch ScoreBoard Screen");
		return true;
	}

}
