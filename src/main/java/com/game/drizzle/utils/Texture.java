package main.java.com.game.drizzle.utils;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES11;
import android.opengl.GLUtils;

public class Texture
{
    private static final String TAG = "Texture";

    private String mFile;
    private int mId;
    private int mHeight;
    private int mWidth;

    public Texture()
    {

    }

    public void setFile(String file) { mFile = file; }
    public String getFile() { return mFile; }
    public void setId(int id) { mId = id; }
    public int getId() { return mId; }
    public void setHeight(int height) { mHeight = height; }
    public int getHeight() { return mHeight; }
    public void setWidth(int width) { mWidth = width; }
    public int getWidth() { return mWidth; }

    public static Texture createTextureFromFile(String file, AssetManager assets)
    {
        Texture texture = null;
        try
        {
            texture = new Texture();
            Bitmap bmp = BitmapFactory.decodeStream(assets.open(file));
            int ids[] = new int[1];
            GLES11.glGenTextures(1, ids, 0);
            int id = ids[0];

            int width = bmp.getWidth();
            int height = bmp.getHeight();

            // all textures must be power of 2
            boolean isPowerOfTwo = (MathHelpers.isPowerOfTwo(width) && MathHelpers.isPowerOfTwo(height));
            if(!isPowerOfTwo)
            {
                throw new Exception("Texture '" + file + "' is not a power of two");
            }

            texture.setFile(file);
            texture.setId(id);
            texture.setWidth(width);
            texture.setHeight(height);

            GLES11.glBindTexture(GLES11.GL_TEXTURE_2D, id);
            GLUtils.texImage2D(GLES11.GL_TEXTURE_2D, 0, bmp, 0);
            GLES11.glTexParameterx(GLES11.GL_TEXTURE_2D, GLES11.GL_TEXTURE_MIN_FILTER, GLES11.GL_NEAREST);
            GLES11.glTexParameterx(GLES11.GL_TEXTURE_2D, GLES11.GL_TEXTURE_MAG_FILTER, GLES11.GL_NEAREST);
            GLES11.glBindTexture(GLES11.GL_TEXTURE_2D, 0);
            bmp.recycle();
        }
        catch (Exception e)
        {
            Logger.logE(TAG, "Error loading texture: " + file);
            Logger.logE(TAG, e.getMessage());
        }
        return texture;
    }
}

