package main.java.com.game.drizzle.utils;

import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import main.java.com.game.drizzle.entities.Entity;

import android.opengl.GLES11;
import android.opengl.GLES11Ext;

public class Renderer {
	// virtual device size
	private final float cVirtualWidth = 1080;
	private final float cVirtualHeight = 1920;
	
	private int mBoundTexture;
	
	private float mRatioWidth;
	private float mRatioHeight;

	public Renderer(float width, float height) {
		mRatioWidth = width / cVirtualWidth;
		mRatioHeight = height / cVirtualHeight;
	}

	public void bindTexture(Texture texture) {
		int id = texture.getId();
		if(mBoundTexture != id) {
			GLES11.glBindTexture(GL11.GL_TEXTURE_2D, id);
		}
	}

	public void draw(Entity e) {
		draw(e.getRegion(), e.getX(), e.getY(), e.getWidth(), e.getHeight());
	}

	public void draw(int[] mRegion, float x, float y, float w, float h) {
		float dx = x * mRatioWidth;
		float dy = y * mRatioHeight;
		float dw = w * mRatioWidth;
		float dh = h * mRatioHeight;
		GLES11.glTexParameteriv(GLES11.GL_TEXTURE_2D, GL11Ext.GL_TEXTURE_CROP_RECT_OES, mRegion, 0);
		GLES11Ext.glDrawTexfOES(dx, dy, 0, dw, dh);
	}

}
