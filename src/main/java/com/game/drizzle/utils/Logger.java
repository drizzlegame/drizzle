package main.java.com.game.drizzle.utils;

public final class Logger
{
    private static boolean mDisabled = false;

    public static void setDisabled(boolean v)
    {
        mDisabled = v;
    }

    public static void logD(String tag, String message)
    {
        if(mDisabled) return;
        android.util.Log.d(tag, message);
    }

    public static void logE(String tag, String message)
    {
        if(mDisabled) return;
        android.util.Log.e(tag, message);
    }
}
