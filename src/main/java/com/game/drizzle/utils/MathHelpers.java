package main.java.com.game.drizzle.utils;

public final class MathHelpers
{
    public static boolean isPowerOfTwo(final int n)
    {
        return ((n != 0) && (n & (n - 1)) == 0);
    }
}
