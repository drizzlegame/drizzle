package main.java.com.game.drizzle.entities;

import main.java.com.game.drizzle.input.TouchEvent;
import android.hardware.SensorEvent;

public class Entity
{
	// Entity position
	private float mX, mY;
	
	// Dimensions
	private int mWidth, mHeight;
	
	// Speed
    private float mSpeedX, mSpeedY;
    
    // If false, entity is not rendered
    private boolean mIsVisible = true;    

    // Entity scale factors
    private float mScaleX = 1f, mScaleY = 1f;
    
    // Region of image to draw
    private int[] mRegion = new int[4];

    public Entity(int x0, int y0, int w, int h, float x, float y)
    {
        mX = x;
        mY = y;
        
        mSpeedX = 0;
        mSpeedY = 0;

        mWidth = w;
        mHeight = h;

        mRegion = createRegion(x0, y0, w, h);
    }

    protected int[] createRegion(int x0, int y0, int w, int h) {
    	return new int[] {x0, y0 + h, w, -h};
	}

    public void update(float dt) {}
    public void processSensorEvent(SensorEvent event) {}
    public boolean processTouchEvent(TouchEvent evt) { return false; }

    public void move()
    {
    	mX += mSpeedX;
    	mY += mSpeedY;
    }
    
	public float getX() {
		return mX;
	}

	public void setX(float x) {
		this.mX = x;
	}
	
	public void setXY(float x, float y)
    {
        mX = x;
        mY = y;
    }

	public float getY() {
		return mY;
	}

	public void setY(float y) {
		this.mY = y;
	}

	public int getWidth() {
		return mWidth;
	}

	public void setWidth(int width) {
		this.mWidth = width;
	}

	public int getHeight() {
		return mHeight;
	}
	
	public float getHeightScaled() {
		return mHeight * mScaleY;
	}
	
	public float getWidthScaled() {
		return mWidth * mScaleX;
	}

	public void setHeight(int height) {
		this.mHeight = height;
	}

	public float getSpeedX() {
		return mSpeedX;
	}

	public void setmSpeedX(float speedX) {
		this.mSpeedX = speedX;
	}

	public float getSpeedY() {
		return mSpeedY;
	}
	
	public void setSpeedXY(float sx, float sy)
    {
        mSpeedX = sx;
        mSpeedY = sy;
    }

	public void setSpeedY(float speedY) {
		this.mSpeedY = speedY;
	}
	
	public void setSpeedX(float speedX) {
		this.mSpeedX = speedX;
	}

	public void addSpeedX(float dx) {
		mSpeedX += dx;
	}
	
	public void addSpeedY(float dy) {
		mSpeedY += dy;
	}
	
	public boolean isVisible() {
		return mIsVisible;
	}

	public void setVisible(boolean isVisible) {
		this.mIsVisible = isVisible;
	}

	public float getScaleX() {
		return mScaleX;
	}

	public void setScaleX(float scaleX) {
		this.mScaleX = scaleX;
	}
	
	public float getScaleY() {
		return mScaleY;
	}

	public void setScaleY(float scaleY) {
		this.mScaleY = scaleY;
	}
	
	public void setScaleXY(float scaleX, float scaleY) {
		this.mScaleX = scaleX;
		this.mScaleY = scaleY;
	}

	public int[] getRegion() {
		return mRegion;
	}
	
}