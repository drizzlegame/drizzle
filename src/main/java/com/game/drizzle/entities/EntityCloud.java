package main.java.com.game.drizzle.entities;

import java.util.Random;

import main.java.com.game.drizzle.manager.GameManager;

public class EntityCloud extends Entity {

	private Random mRnd = new Random();
	private float mTimeToRespawn;
	
	public EntityCloud() {
		super(0, 0, 520, 427, 0, 0);
		randomize();
	}

	@Override
	public void update(float dt) {
		move();
		if(getY() + getHeightScaled() > GameManager.sSurfaceHeight) {
			mTimeToRespawn -= dt;
			if(mTimeToRespawn < 0) {
				randomize();
			}
		}
	}
	
	// Set random scale, speed and time before respawn
	private void randomize() {
		float scale = mRnd.nextFloat() * 2;
		setScaleXY(scale, scale);
		setXY(mRnd.nextInt(GameManager.sSurfaceWidth - 50) + 50, -getHeightScaled());
		setSpeedXY(0, mRnd.nextInt(10) + 5);
		mTimeToRespawn = mRnd.nextFloat() * 2 * 1000;
	}
}
