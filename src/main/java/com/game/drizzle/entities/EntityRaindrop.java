package main.java.com.game.drizzle.entities;

import main.java.com.game.drizzle.manager.GameManager;
import android.hardware.SensorEvent;

public class EntityRaindrop extends Entity
{
    private float mMaxSpeed = 15;
    
    public EntityRaindrop()
    {
        this(0, 0);
    }

    public EntityRaindrop(float x, float y)
    {
        super(0, 0, 125, 209, x, y);
    }

    @Override
    public void update(float dt)
    {
    	move();
    	setSpeedX(getSpeedX() * 0.95f);
    	
    	if(getX() + getWidthScaled() > GameManager.sSurfaceWidth) 
    	{
    		setX(GameManager.sSurfaceWidth - getWidthScaled());
    	}
    	else if (getX() < 0) 
    	{
    		setX(0f);
    	}
    }
    
    @Override
    public void processSensorEvent(SensorEvent event) 
    {
    	float acc = -event.values[0];
    	if(acc > -2.5 && acc < 2.5) 
    	{
    		acc = 0;
    	}
    	
    	addSpeedX(acc / 35f);
    	if(getSpeedX() > mMaxSpeed) setSpeedX(mMaxSpeed);
    	if(getSpeedX() < -mMaxSpeed) setSpeedX(-mMaxSpeed);
    }

}
