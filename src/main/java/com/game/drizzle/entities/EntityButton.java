package main.java.com.game.drizzle.entities;

import main.java.com.game.drizzle.input.TouchEvent;
import android.view.MotionEvent;

public abstract class EntityButton extends Entity {

	public EntityButton(int x0, int y0, int w, int h, float x, float y) {
		super(x0, y0, w, h, x, y);
	}
	
	@Override
	public boolean processTouchEvent(TouchEvent event) {
		if(event.action == MotionEvent.ACTION_DOWN || event.action == MotionEvent.ACTION_UP) {
			boolean handled = false;
			float x = event.virtualX;
			float y = event.virtualY;
			float x0 = getX(), x1 = x0 + getWidth();
			float y0 = getY(), y1 = y0 + getHeight();
			if(x > x0 && x < x1 && y > y0 && y < y1) {
				handled = event.action == MotionEvent.ACTION_DOWN ? onPressed() : onReleased();
				if(handled) return true;
			}
		}
		return false;
	}
	
	protected boolean onPressed() { return false; }
	protected boolean onReleased() { return false; }
}
