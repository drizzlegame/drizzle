package main.java.com.game.drizzle.entities;

import javax.microedition.khronos.opengles.GL11Ext;

import main.java.com.game.drizzle.manager.GameManager;
import main.java.com.game.drizzle.utils.Renderer;
import android.opengl.GLES11;
import android.opengl.GLES11Ext;

public class EntityNumbers extends Entity {
	
	private int[][] mNumber = new int[10][4];
	private int mValue;
	private int[] mDigits = new int[20];
	
	public EntityNumbers() {
		super(0, 0, 34, 42, 50, GameManager.sSurfaceHeight - 80 - 50);
		extractNumbers();
	}

	private void extractNumbers() {
		int offset = 1;
		for(int i = 0; i < 10; i++) {
			mNumber[i] = createRegion(offset, 208, getWidth(), getHeight());
			offset += 35;
		}
	}
	
	public void draw(Renderer renderer) {
		int number = getValue(), position = 0;
		while (number > 0) {
		    int digit = number % 10;
		    number = number / 10;
		    mDigits[position++] = digit;
		}

		float x = getX(), y = getY();
		for(int i = 0; i < position; i++) {
			renderer.draw(mNumber[mDigits[position - 1 - i]], x + i * 80 + i * 5, y, 80, 80);
		}
	}
	
	public void setValue(int value) {
		mValue = value;
	}

	public int getValue() {
		return mValue;
	}

	public void addValue(int i) {
		mValue += i;
	}

}
