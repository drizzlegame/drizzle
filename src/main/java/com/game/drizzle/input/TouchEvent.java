package main.java.com.game.drizzle.input;

import main.java.com.game.drizzle.manager.GameManager;
import main.java.com.game.drizzle.utils.Logger;
import android.view.MotionEvent;

public class TouchEvent {
	public float deviceX, deviceY, virtualX, virtualY;
	public int action;
	
	public TouchEvent(MotionEvent event) {
		deviceX = event.getX();
		deviceY = event.getY();
		action = event.getAction();
		virtualX = deviceX * (1080f / GameManager.sSurfaceWidth);
		virtualY = 1920f - deviceY * (1920f / GameManager.sSurfaceHeight);
		//Logger.logD("TOUCH", deviceX + "," + deviceY + " -> " + virtualX + "," + virtualY);
	}

}
