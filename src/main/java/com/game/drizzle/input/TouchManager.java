package main.java.com.game.drizzle.input;

import java.util.ArrayList;
import java.util.List;

import android.view.MotionEvent;

public class TouchManager
{
	private List<TouchEvent> mBufferedEvents = new ArrayList<TouchEvent>();
	private List<TouchEvent> mProcessedEvents = new ArrayList<TouchEvent>();
	
	public synchronized void addEvent(MotionEvent event) 
	{
		mBufferedEvents.add(new TouchEvent(event));
	}
	
	public synchronized List<TouchEvent> getEvents()
	{
		mProcessedEvents.addAll(mBufferedEvents);
		mBufferedEvents.clear();
		return mProcessedEvents;
		//return mBufferedEvents;
	}

	public void clear() {
		mProcessedEvents.clear();
	}

}
