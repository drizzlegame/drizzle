package main.java.com.game.drizzle.input;

import java.util.ArrayList;

// buffers and pools accelerometer events
public class InputManager<T>
{
	private ArrayList<T> mBufferedEvents = new ArrayList<T>();
	private ArrayList<T> mProcessedEvents = new ArrayList<T>();
	
	
	public synchronized void addEvent(T event) 
	{
		mBufferedEvents.add(event);
	}
	
	public synchronized ArrayList<T> getEvents()
	{
		//mProcessedEvents.addAll(mBufferedEvents);
		//mBufferedEvents.clear();
		//return mProcessedEvents;
		return mBufferedEvents;
	}

	public void clear() {
		mBufferedEvents.clear();
	}

}
