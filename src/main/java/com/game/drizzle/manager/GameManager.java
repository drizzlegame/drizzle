package main.java.com.game.drizzle.manager;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import main.java.com.game.drizzle.MainActivity;
import main.java.com.game.drizzle.input.InputManager;
import main.java.com.game.drizzle.input.TouchEvent;
import main.java.com.game.drizzle.input.TouchManager;
import main.java.com.game.drizzle.utils.Renderer;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.opengl.GLES11;
import android.view.MotionEvent;

public class GameManager
{
	private static GameManager instance = null;
	
    public static int sDeviceWidth, sDeviceHeight;
	public static int sSurfaceWidth, sSurfaceHeight;

    private Screen mCurrentScreen;
    private InputManager<SensorEvent> mSensorInputManager = new InputManager<SensorEvent>();
    private TouchManager mTouchInputManager = new TouchManager();
    private Renderer mRenderer;
    
    private float mFrameTime = 0f;

	private Context mContext;
    
    protected GameManager() {
    	
    }
    
    public static GameManager getInstance() {
    	if (instance == null) {
    		instance = new GameManager();
    	}
    	return instance;
    }

    public Screen getCurrentScreen() {
		return mCurrentScreen;
	}

	public void setCurrentScreen(Screen mCurrentScreen) {
		this.mCurrentScreen = mCurrentScreen;
	}

	public void load(int width, int height)
    {
    	sSurfaceWidth = width;
        sSurfaceHeight = height;
        
        //GLES11.glViewport(0, 0, width, height);
        
        mRenderer = new Renderer(width, height);
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // Optimize OpenGL for 2D
        GLES11.glDisable(GL11.GL_DITHER);
        GLES11.glDisable(GL11.GL_ALPHA_TEST);
        GLES11.glDisable(GL11.GL_STENCIL_TEST);
        GLES11.glDisable(GL11.GL_FOG);
        GLES11.glDisable(GL11.GL_LIGHTING);
        GLES11.glDisable(GL11.GL_CULL_FACE);
        GLES11.glDisable(GL11.GL_DEPTH_TEST);
        GLES11.glDisable(GL11.GL_MULTISAMPLE);
        GLES11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
        GLES11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
        GLES11.glEnable(GL11.GL_TEXTURE_2D);
        GLES11.glEnable(GL11.GL_BLEND);
        GLES11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
    }
	
	public void initializeScreen() {
		mCurrentScreen.initializeScreen(mContext);
	}

	public void handleInput(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) 
		{ 
			mSensorInputManager.addEvent(event);
		}
	}

	public void handleInput(MotionEvent event) {
		mTouchInputManager.addEvent(event);
	}

	public void onDrawFrame() 
	{
		/* Game loop */
		long mStartTime = System.currentTimeMillis();
		
		// process input
		List<SensorEvent> sensor = mSensorInputManager.getEvents();
		if(!sensor.isEmpty()) {
			mCurrentScreen.handleSensorInput(sensor);
			mSensorInputManager.clear();
		}
		List<TouchEvent> touch = mTouchInputManager.getEvents();
		if(!touch.isEmpty()) {
			mCurrentScreen.handleTouchInput(touch);
			mTouchInputManager.clear();
		}
		
		// update
		mCurrentScreen.updateScreen(mFrameTime);
		
		// draw
		mCurrentScreen.drawToScreen(mRenderer);
		
		mFrameTime = System.currentTimeMillis() - mStartTime;
	}

	public void onBackPressed() {
		mCurrentScreen.handleBackEvent();
	}

	public void setContext(Context mContext) {
		this.mContext = mContext;
	}
}
