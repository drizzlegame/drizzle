package main.java.com.game.drizzle.manager;

import java.util.List;

import main.java.com.game.drizzle.input.TouchEvent;
import main.java.com.game.drizzle.utils.Renderer;
import android.content.Context;
import android.hardware.SensorEvent;
import android.view.MotionEvent;

public interface Screen {

	/*
	 * Initialize screen is called upon screen start up, whether it be from the load of the game (main menu)
	 * or a different screen. E.g. Main menu to play screen.
	 * 
	 * @param context The context passed to it from the previous screen
	 */
	public void initializeScreen(Context context);
	
	/*
	 * Called when the screen is going to be destroyed.
	 * Unload and kill any elements here.
	 */
	public void unloadScreen();
	public void drawToScreen(Renderer mRenderer);
	public void updateScreen(float dt);
	public void handleSensorInput(List<SensorEvent> sensor);
	public void handleTouchInput(List<TouchEvent> touch);

	public void handleBackEvent();

}
